extends Node

## 全局Button的鼠标点击音效，需要加入项目的Autooad，且需要最先加载
## 这里没有tscn + gd，而使用纯gd的方案，主要是因为如果使用前者那么需要同时维护一个tscn场景和一个gd脚本，不过根据官方文档的描述：
## https://docs.godotengine.org/en/stable/tutorials/best_practices/scenes_versus_scripts.html#performance-of-script-vs-packedscene
## 场景的性能要高于脚本，因此如果性能放在首位的话，应该使用tscn + gd的方案

## 点击音效
const audio_stream: AudioStream = preload("res://snd/000.wav")

## 播放器
var audio_player: AudioStreamPlayer = AudioStreamPlayer.new()

func _init():
	audio_player.name = "AudioPlayer"
	audio_player.stream = audio_stream
	
	add_child(audio_player)
	
func _enter_tree():
	get_tree().node_added.connect(_on_node_added)
	
func _exit_tree():
	get_tree().node_added.disconnect(_on_node_added)

func _on_node_added(node):
	if node is BaseButton:
		node.pressed.connect(_on_Button_pressed.bind(node))

func _on_Button_pressed(_button: BaseButton):
	audio_player.play()
