extends Camera2D
class_name GameCamera

## 具有鼠标滚轮缩放、左键移动和聚焦功能的2D摄像机


## 摄像机最大缩放值
@export 
var max_zoom: float = 1.5
## 摄像机最小缩放值
@export 
var min_zoom: float = 0.543
## 摄像机缩放步进
@export var zoom_step: float = 0.1
## 摄像机缩放速度
@export var zoom_rate: float = 8.0

## 目标缩放
var _target_zoom: float = 1.0


func _physics_process(delta: float) -> void:
	zoom = lerp(zoom, _target_zoom * Vector2.ONE, zoom_rate * delta)
	set_physics_process(not is_equal_approx(zoom.x, _target_zoom))

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if event.button_mask == MOUSE_BUTTON_MASK_LEFT:
			var x = get_viewport().get_visible_rect().size.x / 2
			var y = get_viewport().get_visible_rect().size.y / 2
			position.x = clampf(position.x - event.relative.x, limit_left + x, limit_right - x)
			position.y = clampf(position.y - event.relative.y, limit_top + y, limit_bottom - y)
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				zoom_in()
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				zoom_out()
			if event.double_click:
				focus_position(get_global_mouse_position())
	
## 摄像机放大
func zoom_in() -> void:
	_target_zoom = min(_target_zoom + zoom_step, max_zoom)
	set_physics_process(true)

## 摄像机缩小
func zoom_out() -> void:
	_target_zoom = max(_target_zoom - zoom_step, min_zoom)
	set_physics_process(true)

## 将摄像机聚焦到 [code]target_position[/code]
func focus_position(target_position: Vector2) -> void:
	var tween = create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_SINE)
	
	tween.tween_property(self, "position", target_position, 0.2)
